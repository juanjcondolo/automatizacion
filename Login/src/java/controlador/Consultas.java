package dat;

import java.sql.PreparedStatement;
import java.sql.ResultSet;

/**
 *
 * @author Febre
 */
public class Consultas extends Conexion {

    Conexion cone = new Conexion();

    // Consultar
    public boolean autenticar(String usuarioI, String passI) {
        PreparedStatement pre = null;
        ResultSet rs = null;
        try {
            String sentencia = "select * from estudiantes where usuario = ? and pass  = ? ";
            pre = getConnection().prepareStatement(sentencia);

            pre.setString(1, usuarioI);
            pre.setString(2, passI);
            rs = pre.executeQuery();

            // Si se ejecuta retorna verdadero
            if (rs.absolute(1)) {
                return true;

            }
        } catch (Exception e) {
            System.err.println(e.getMessage());
        }
        return false;
    }

    public boolean autenticar2(String usuarioI, String passI) {
        PreparedStatement pre = null;
        ResultSet rs = null;
        try {
            String sentencia = "select * from tutor_academico where usuario = ? and pass  = ? ";
            pre = getConnection().prepareStatement(sentencia);

            pre.setString(1, usuarioI);
            pre.setString(2, passI);
            rs = pre.executeQuery();

            // Si se ejecuta retorna verdadero
            if (rs.absolute(1)) {
                return true;

            }
        } catch (Exception e) {
            System.err.println(e.getMessage());
        }
        return false;
    }

    public boolean autenticar3(String usuarioI, String passI) {
        PreparedStatement pre = null;
        ResultSet rs = null;
        try {
            String sentencia = "select * from tutor_externo where usuario = ? and pass  = ? ";
            pre = getConnection().prepareStatement(sentencia);

            pre.setString(1, usuarioI);
            pre.setString(2, passI);
            rs = pre.executeQuery();

            // Si se ejecuta retorna verdadero
            if (rs.absolute(1)) {
                return true;

            }
        } catch (Exception e) {
            System.err.println(e.getMessage());
        }
        return false;
    }
}
