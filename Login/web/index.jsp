<%-- 
    Document   : index
    Created on : 10-dic-2018, 21:42:08
    Author     : Febre
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>Iniciar Sesión</title>
        <link rel="stylesheet" type="text/css" href="css/estilo.css">
        
    </head>
    <body>

        <!--Creamos formulario-->
        <div class = "login-box">
            <img src="img/logo.png" class="avatar" alt="Avatar Image">
            <h1>Iniciar Sesión</h1>
            <form action="iniciar" method= "post" >
                <input type="text" name = "usuario" placeholder="Usuario"> <br>
                <input type="password" name = "pass" placeholder="Contraseña"> <br>
                <input type="submit" value="Ingresar"> <br>
                <a href="empresas.jsp">Empresas </a>
            </form>
        </div>
    </body>
</html>
